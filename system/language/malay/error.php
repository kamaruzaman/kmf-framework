<?php

$lang['general_error']				= "Maaf, terdapat masalah serius ditemui pada file \"%s\" di baris %s";
$lang['class_not_found']			= "File atau class yang bernama %s tidak dapat ditemui";
$lang['class_not_load']				= "Class %s not loaded. <br /> Penggunaan %s pada fail \"%s\" di baris %s";
$lang['undefined_variable']			= "Pemboleh ubah %s tidak diketahui. <br /> Penggunaan %s pada fail \"%s\" di baris %s";
$lang['undefined_method']			= "Fungsi %s tidak diketahui. <br /> Penggunaan %s pada fail \"%s\" di baris %s";
$lang['declare_to_empty_object']	= "Menyatakan nilai pada objek yang tidak wujud. <br /> Penggunaan pada fail \"%s\" di baris %s";
$lang['redeclare_function']     	= "Terdapat fungsi berulang %s. <br /> Penggunaan %s pada fail \"%s\" di baris %s";
$lang['object_cant_convert']     	= "Objek dari class %s tidak boleh di tukar ke jenis data %s. <br /> Penggunaan pada fail \"%s\" di baris %s";
$lang['syntax_error']    			= "Ralat \"syntax\", tidak menjangka %s, bahkan menyangka %s. <br /> Penggunaan pada fail \"%s\" di baris %s";
$lang['missing_argument']    		= "Pemboleh ubah %s untuk fungsi %s tidak ditemui. <br /> Penggunaan pada fail \"%s\" di baris %s";

?>