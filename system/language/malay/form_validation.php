<?php

$lang['required']			= "Ruangan %s mestilah di isi.";
$lang['isset']				= "Ruangan %s seharusnya mempunyai nilai.";
$lang['valid_email']		= "Ruangan %s seharusnya mempunyai alamat email yang sah.";
$lang['valid_emails']		= "Ruangan %s field must contain all valid email addresses.";
$lang['valid_url']			= "Ruangan %s seharusnya mempunyai URL yang sah.";
$lang['valid_ip']			= "Ruangan %s seharusnya mempunyai alamat IP yang sah.";
$lang['min_length']			= "Ruangan %s seharusnya mempunyai jumlah karakter sekurang kurangnya %s karakter.";
$lang['max_length']			= "Ruangan %s seharusnya mempunyai jumlah karakter tidak lebih daripada %s karakter.";
$lang['exact_length']		= "Ruangan %s field must be exactly %s characters in length.";
$lang['alpha']				= "Ruangan %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "Ruangan %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "Ruangan %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "Ruangan %s field must contain only numbers.";
$lang['is_numeric']			= "Ruangan %s field must contain only numeric characters.";
$lang['integer']			= "Ruangan %s field must contain an integer.";
$lang['regex_match']		= "Ruangan %s seharusnya mempunyai format yang betul.";
$lang['matches']			= "Ruangan %s field does not match Ruangan %s field.";
$lang['is_unique'] 			= "Ruangan %s field must contain a unique value.";
$lang['is_natural']			= "Ruangan %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "Ruangan %s field must contain a number greater than zero.";
$lang['decimal']			= "Ruangan %s field must contain a decimal number.";
$lang['less_than']			= "Ruangan %s field must contain a number less than %s.";
$lang['greater_than']		= "Ruangan %s field must contain a number greater than %s.";

?>