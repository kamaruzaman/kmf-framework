<?php

class kmf{

	public function load_sys_class($filepath, $args = array()) {
		if(@file_exists($filepath)) {
			$class_name = basename($filepath);
			$class_name = explode(".", $class_name);
	        $class_declare = $class_name[0];
	        if(!class_exists($class_declare))
	        	require_once($filepath);	
	        if(sizeof($args) > 0) {
	        	$this->$class_declare = new $class_declare($args);
	        } else {
	        	$this->$class_declare = new $class_declare();
	        }
		} else {
			die("Load system class failed, file not found: ".$filepath);
		}
	}

	public function load_class($class_name, $args = array()) {
		$filepath = CLASS_PATH.$class_name.'.php';
		if(@file_exists($filepath)) {
			$class_name = basename($filepath);
			$class_name = explode(".", $class_name);
	        $class_declare = $class_name[0];
	        if(!class_exists($class_declare))
	        	require_once($filepath);	
	        if(sizeof($args) > 0) {
	        	return new $class_declare($args);
	        } else {
	        	return new $class_declare();
	        }
		} else {
			die("Load system class failed, file not found: ".$filepath);
		}
	}

	public function redirect_header($location) {
	 @header("Location: ".$location);
	}

	public function redirect_meta($location, $time = 0, $msg = false) {
		if($msg) {
			return '<a class="meta-redirect" href="'.$location.'">'.$msg.'</a><META http-equiv="refresh" content="'.$time.';URL='.$location.'">';
		} else {
			return '<META http-equiv="refresh" content="'.$time.';URL='.$location.'">';
		}
	}

	public function autoload() {
		global $autoload, $config,$lang;
		$this->locale();
		//set_error_handler('error_function', E_ALL);
		//register_shutdown_function('fatalErrorHandler');
		if(is_array($autoload) && sizeof($autoload) > 0) {
			foreach($autoload as $load) {
				$file_path = CLASS_PATH.'/'.$load.'.php';
				if(@file_exists($file_path)) {
					if(array_key_exists($load, $config)){
						$this->load_sys_class($file_path, $config[$load]);
					} else {
						$this->load_sys_class($file_path);
					}
					
				} else {
					$message = sprintf($lang['class_not_found'],$load);
					error_page($message);
					die();
				}
			}
		}
	}

	public function locale(){
		global $config,$lang;
		$language = $config['language'];
		$lang_dir = SYS_PATH."system/language/".$language."/";
		$files = scandir($lang_dir);
		foreach($files as $file){
			if($file != "." && $file != ".."){
				if(substr($file, strlen($file)-4) == ".php") {
					require_once($lang_dir.$file);
				}
			}
		}
	}

}

function fatalErrorHandler()
{
	global $lang;
    $error = error_get_last();
    //var_dump($error);
 
    if(count($error) > 0)
    {
    	if(preg_match('/Call to undefined method stdClass::(.+)/',$error['message'],$matches)){
			$message = sprintf($lang['undefined_method'],$matches[1],$matches[1],$error['file'],$error['line']);
			error_page($message);
		} else if(preg_match('/Creating default object from empty value/',$error['message'],$matches)){
			$message = sprintf($lang['declare_to_empty_object'],$error['file'],$error['line']);
			error_page($message);
		} else if(preg_match('/Cannot redeclare (.+)/',$error['message'],$matches)){
			$message = sprintf($lang['redeclare_function'],$matches[1],$matches[1],$error['file'],$error['line']);
			error_page($message);
		} else if(preg_match('/syntax error, unexpected (.+), expecting (.+)/',$error['message'],$matches)){
			$message = sprintf($lang['syntax_error'],$matches[1],$matches[2],$error['file'],$error['line']);
			error_page($message);
		} else {
			$message = sprintf($lang['general_error'],$error['file'],$error['line']);
			error_page($message);
		}
        
    }
    die();
}

function error_function($error_level,$error_message,$error_file,$error_line,$error_context) {
	global $lang;
	if(preg_match('/Undefined property: kmf::\$(.+)/',$error_message,$matches)){
		$message = sprintf($lang['class_not_load'],$matches[1],$matches[1],$error_file,$error_line);
		error_page($message);
	}
	else if(preg_match('/Undefined variable:(.+)/',$error_message,$matches)){
		$message = sprintf($lang['undefined_variable'],$matches[1],$matches[1],$error_file,$error_line);
		error_page($message);
	} else if(preg_match('/Object of class (.+) could not be converted to (.+)/',$error_message,$matches)){
		$message = sprintf($lang['object_cant_convert'],$matches[1],$matches[2],$error_file,$error_line);
		error_page($message);
	} else if(preg_match('/Missing argument (.+) for (.+),/',$error_message,$matches)){
		$message = sprintf($lang['missing_argument'],$matches[1],$matches[2],$error_file,$error_line);
		error_page($message);
	}  else {
		$message = sprintf($lang['general_error'],$error_file,$error_line);
		error_page($message);
	} //var_dump($error_message);
	die();
}

function error_page($message){
	global $config;
	$heading = "Error Message";
	include(SYS_PATH."system/error/general_error.php");

}

?>