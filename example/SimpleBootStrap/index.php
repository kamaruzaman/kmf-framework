<?php
// mysql error
// Query failed: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '%s, %s, %s)' at line 1
require('../../config/core.php');

$kmf->UploadHelper->config = array(
	'upload_path' => './upload',
	'allowed_types' => 'jpg|php|rar',
	'max_size' => '2000000',
	'overwrite' => false,
	'encrypt_name' => true,
	'remove_spaces' => false,
	'return_type' => 'json'
	);

function check_username($str) {
	global $kmf;
	if($str != "Ayam") {
		$kmf->Validation->set_error_message('username', 'Username mesti "Ayam".');
		return false;
	}
}

$kmf->Validation->set_rules('username', 'Username', 'required|callback[check_username]');
$kmf->Validation->set_rules('password', 'Password', 'required|max_length[10]|min_length[5]');
$kmf->Validation->set_rules('email', 'Email', 'required|max_length[20]|min_length[10]');
$kmf->Validation->set_rules('fullname', 'Full Name', 'required|max_length[10]|min_length[4]');
$kmf->Validation->set_rules('phoneno', 'Phone number', 'required');

if($kmf->Validation->run() && $kmf->Validation->get_status() == TRUE) {
	
}

if($_POST){
	$kmf->UploadHelper->upload('file');
	if($kmf->UploadHelper->progressUpload()->success) {
		echo "Success lah!";
		var_dump($kmf->UploadHelper->data());
	} else {
		echo "Failed lah :(";
		echo "<p>".$kmf->UploadHelper->getError()."</p>";
	}
}

$kmf->BootTheme->htmltitle("Contoh Laman");

$kmf->BootTheme->navbar(array(
							'brand' => 'Website',
							'brand_link' => 'url1',
							'name' => 'navbar1',
							'class' => 'navbar-fixed-top',
							'navs' => array(
										array('title' => 'link','link' => 'url2', 'class' => 'active'),
										array('title' => 'link','link' => 'url3'),
										array('title' => 'Dropdown','dropdown' => array(
													array('title' => 'link','link' => 'abc'),
													array('title' => 'link','link' => 'def'),
													array('divider' => true),
													array('title' => 'nav header','header' => true),
													array('title' => 'link','link' => 'ghi')
												)
											)
									)
						));


$kmf->BootTheme->div(array('class' => 'container'));

$kmf->BootTheme->navbar(array(
						'brand' => 'Website',
						'brand_link' => 'url1',
						'name' => 'navbar2',
						'navs' => array(
									array('title' => 'link','link' => 'url2', 'class' => 'active'),
									array('title' => 'link','link' => 'url3'),
									array('title' => 'Dropdown','dropdown' => array(
												array('title' => 'link','link' => 'abc'),
												array('title' => 'link','link' => 'def'),
												array('divider' => true),
												array('title' => 'nav header','header' => true),
												array('title' => 'link','link' => 'ghi')
											)
										)
								)
					));

$kmf->BootTheme->headings('h3', 'Callback Element');

// Assign as callback
$kmf->BootTheme->button(array('value' => 'test', 'callback' => 'buttontest'));
$kmf->BootTheme->button(array('value' => 'ayam', 'callback' => 'buttontest2'));
$kmf->BootTheme->pre('Pedot','','prepedot');

// Use callback
$kmf->BootTheme->p('saya {buttontest} makan nasi {buttontest2} {prepedot}');
$kmf->BootTheme->code($kmf->BootTheme->cleantext(
						sprintf('%s<br />%s<br />%s<br />%s',
							"\$kmf->BootTheme->button(array('value' => 'test', 'callback' => 'buttontest'));",
							"\$kmf->BootTheme->button(array('value' => 'ayam', 'callback' => 'buttontest2'));",
							"\$kmf->BootTheme->pre('Pedot','','prepedot');",
							"\$kmf->BootTheme->p('saya {buttontest} makan nasi {buttontest2} {prepedot}');")
					));

$kmf->BootTheme->br();

 $kmf->BootTheme->headings('h3', 'Form Validation');
 $kmf->BootTheme->form_open(array('action' => '','enctype' => true, 'class' => 'form-horizontal'));
 $kmf->BootTheme->form('input','Username', array('placeholder' => 'Username','type' => 'text', 'name' => 'username'));
 $kmf->BootTheme->form('input','Password', array('placeholder' => 'Password','type' => 'password', 'name' => 'password'));
 $kmf->BootTheme->form('input','Email', array('placeholder' => 'Email','type' => 'email', 'name' => 'email'));
 $kmf->BootTheme->form('input','Full Name', array('placeholder' => 'Full Name','type' => 'text', 'name' => 'fullname'));
 $kmf->BootTheme->form('input','Phone Number', array('placeholder' => 'Phone Number','type' => 'text', 'name' => 'phoneno'));
 $kmf->BootTheme->form('fileinput','Profile Pic', array('name' => 'file'));
 $kmf->BootTheme->form('fileinput','Multiple', array(
 													'name' => 'file2',
 													'multiple' => true,
 													'style' => 'well',
 													'btn_choose' => 'Drop files here or click to choose2',
													'droppable' => 'false',
 													));
 $kmf->BootTheme->form('submit');
 $kmf->BootTheme->form_close();
 $kmf->BootTheme->hr();

 $kmf->BootTheme->headings('h3', 'Example from array');
 $kmf->BootTheme->tables(array('class' => 'table table-striped table-bordered table-hover table-condensed', 'set_empty' => 'No data', 'function' => 'strtoupper',  'heading' => array('Name', 'Age', 'Phone Number'),'rowsdata' => array(array('Maro', '', '0121234567'), array('Maro', '13', '0121234567'), array('Maro', '13', '0121234567'), array('Maro', '13', '0121234567'), array('Maro', '13', '0121234567'), array('Maro', '13', '0121234567'), array('Maro', '13', '0121234567'))));
 $kmf->BootTheme->headings('h3', 'Example from database');
 //$kmf->BootTheme->tables(array('class' => 'table table-striped table-bordered', 'function' => 'strtolower', 'set_empty' => 'No data found',  'heading' => array('Username', 'Password'), 'rowsdata' => $kmf->FreeSQL->query("SELECT username, password FROM users")));

 $kmf->BootTheme->headings('h3', 'Form Element');
 $kmf->BootTheme->form_open(array('action' => 'http://localhost','enctype' => true, 'class' => 'form-horizontal'));
 $kmf->BootTheme->form('input','test',array('value' => 'test', 'placeholder' => 'test','type' => 'number'));
 $kmf->BootTheme->form('textarea','textarea',array('value' => 'test', 'name' => 'test'));
 $kmf->BootTheme->form('checkbox','checkbox',array('datas' => array(
 															array('label' => 'label', 'value' => 'value', 'name' => 'name'),
 															array('label' => 'label2', 'value' => 'value2', 'name' => 'name2')
 														)));
 $kmf->BootTheme->form('checkbox','checkbox inline',array('datas' => array(
 															array('label' => 'label', 'value' => 'value', 'name' => 'name'),
 															array('label' => 'label2', 'value' => 'value2', 'name' => 'name2')
 														),
 											'inline' => true));
 $kmf->BootTheme->form('checkbox','checkbox single',array('label' => 'label', 'value' => 'value', 'name' => 'name'));
 $kmf->BootTheme->form('radio','radio',array('datas' => array(
 															array('label' => 'label', 'value' => 'value', 'name' => 'name'),
 															array('label' => 'label2', 'value' => 'value2', 'name' => 'name2')
 														)));
 $kmf->BootTheme->form('radio','radio inline',array('datas' => array(
 															array('label' => 'label', 'value' => 'value', 'name' => 'name'),
 															array('label' => 'label2', 'value' => 'value2', 'name' => 'name2')
 														),
 											'inline' => true));
 $kmf->BootTheme->form('radio','radio single',array('label' => 'label', 'value' => 'value', 'name' => 'name'));
 $kmf->BootTheme->form('select','select',array('name' => 'nama', 'class' => 'ajik', 'data' => array('value','label','value1','label1','value2','label2'), 'selected' => 'value2'));
 $kmf->BootTheme->form_close();

 
 $kmf->BootTheme->div_close();
 $kmf->BootTheme->build();

?>
