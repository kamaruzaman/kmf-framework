<?php
require('../../config/core.php');

$userform = $kmf->load_class('UserForm');

$userform->config = array(
	'query' => "INSERT INTO users (username, password) VALUES ('{username}','{password}')",
	'form' => array('action' => '','enctype' => true, 'class' => 'form-horizontal'),
	'fields' => array(
		array('type' => 'input', 'fieldname' => 'Username', array('placeholder' => 'Username','type' => 'text', 'name' => 'username', 'rules' => 'required|max_length[100]|min_length[2]|callback[check_username]')),
		array('type' => 'input', 'fieldname' => 'Password', array('placeholder' => 'Password','type' => 'text', 'name' => 'password', 'rules' => 'required|max_length[10]|min_length[5]')),
		array('type' => 'submit'),
		),
);

$userform->renderForm();
if($userform->success) {
	echo "Data dimasukkan ".$userform->getInsertID()."";
}

function check_username($str) {
	global $kmf;
	if($str != "cicaks") {
		$kmf->Validation->set_error_message('username', 'Username mesti "Ayams".');
		return false;
	}
}

?>