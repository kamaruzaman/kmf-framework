<?php

$config['base_url'] = "http://localhost/freelancer/";

$config['BootTheme']['css_path'] = array($config['base_url'].'assets/css/bootstrap.min.css',
										$config['base_url'].'assets/css/font-awesome.min.css',
										$config['base_url'].'assets/css/kmf.css');
$config['BootTheme']['js_path'] = array($config['base_url'].'assets/js/jquery.js',
										$config['base_url'].'assets/js/bootstrap.min.js',
										$config['base_url'].'assets/js/kmf.js');

$config['language'] = "malay";


?>