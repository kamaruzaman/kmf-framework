<?php

class UserAuth{

	
	public $default_checked_session_key  = 'logged';
	public $authdata = array();

	public function is_auth() {
		if(isset($_SESSION[$this->default_checked_session_key])) {
			return true;
		}
		return false;
	}

	public function set_auth() {
		if(is_array($this->authdata)) {
			$_SESSION[$this->default_checked_session_key] = true;
			foreach($this->authdata as $array_key => $array_value) {
				$_SESSION[$array_key] = $array_value;
			}
			return true;
		}
		return false;
	}

	public function set_data($sessionkey, $sessionval) {
		$_SESSION[$sessionkey] = $sessionval;
	}

	public function unset_auth() {
		if(is_array($this->authdata)) {
			unset($_SESSION[$this->default_checked_session_key]);
			foreach($this->authdata as $array_key => $array_value) {
				unset($array_key);
			}
			session_destroy();
			return true;
		}
		return false;
	}

	public function unset_session($sessionkey) {
		unset($_SESSION[$sessionkey]);
	}

	public function isset_session($sessionkey) {
		return isset($_SESSION[$sessionkey]);
	}

	public function get_data($sessionkey) {
		return $_SESSION[$sessionkey];
	}
	
	
}

?>