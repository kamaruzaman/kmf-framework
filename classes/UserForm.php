<?php

class UserForm{
	public $config = array();
	private $return = "";
	public $status = false;
	public $success = false;
	private $fields = array();

	public function __construct() {

	}

	public function renderForm() {
		global $kmf;

		if(!isset($this->config['query'])) {
			die("Please declare the 'query' argument");
		}

		if(isset($this->config['fields'])) {

			for($i = 0; $i < sizeof($this->config['fields']); $i++) {
				if(isset($this->config['fields'][$i]['type'])) {
					if(isset($this->config['fields'][$i]['fieldname'])) {
						$this->fields[] = $this->config['fields'][$i][0]['name'];
						$kmf->Validation->set_rules($this->config['fields'][$i][0]['name'], $this->config['fields'][$i]['fieldname'], $this->config['fields'][$i][0]['rules']);
					}
				}
			}

			if($kmf->Validation->run() && $kmf->Validation->get_status() == TRUE) {
				if($kmf->FreeSQL->query($this->buildQuery())) {
					$this->success = true;
				}
			}
			

		}

		$this->parseConfig();
		$this->buildTheForm();
	}

	public function getInsertID() {
		global $kmf;
		return $kmf->FreeSQL->lastInsertID();
	}

	private function buildQuery() {
		global $kmf;
		if(sizeof($this->fields) > 0) {
			$build_query = $this->config['query'];
			foreach($this->fields as $field) {
				$build_query = preg_replace("/\{$field\}/", $kmf->FreeSQL->safe_sql($kmf->Validation->_field_data[$field]['postdata']), $build_query);
			}
			return $build_query;
		}
	}

	private function parseConfig() {
		global $kmf;

		if(!isset($this->config['form'])) {
			die("Please declare the 'form' argument");
		}

		if(isset($this->config['fields'])) {
			$kmf->BootTheme->form_open($this->config['form']);
			for($i = 0; $i < sizeof($this->config['fields']); $i++) {
				if(isset($this->config['fields'][$i]['type'])) {
					if(isset($this->config['fields'][$i]['fieldname'])) {
						$kmf->BootTheme->form($this->config['fields'][$i]['type'], $this->config['fields'][$i]['fieldname'], $this->config['fields'][$i][0]);
						$kmf->Validation->set_rules($this->config['fields'][$i][0]['name'], $this->config['fields'][$i]['fieldname']);
					} else {
						$kmf->BootTheme->form($this->config['fields'][$i]['type']);
					}
					$this->status = true;
				}
			}
			$kmf->BootTheme->form_close();
		}
	}

	private function buildTheForm() {
		global $kmf;
		if($this->status) {
			$kmf->BootTheme->build();
		}
	}
}

?>