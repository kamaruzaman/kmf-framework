<?php

class Validation{
	

	public $return_status = TRUE;
	public $_field_data			= array();
	public $_error_array			= array();
	public $_error_messages		= array();
	public $error_string			= '';
	public $_safe_form_data		= FALSE;
	public $error_wrapper_open = "<span class='help-block'>";
	public $error_wrapper_close = "</span>";

	public function data(){
		return $this;
	}

	public function get_status(){
		return $this->return_status;
	}

	public function set_rules($field_name, $field_display = '', $valid_rules = "") {

		if ( ! is_string($field_name) OR  ! is_string($valid_rules) OR $field_name == '')
		{
			return $this;
		}

		$field_display = ($field_display == '') ? $field_name : $field_display;

		if (strpos($field_name, '[') !== FALSE AND preg_match_all('/\[(.*?)\]/', $field_name, $matches))
		{
			$x = explode('[', $field_name);
			$indexes[] = current($x);

			for ($i = 0; $i < count($matches['0']); $i++)
			{
				if ($matches['1'][$i] != '')
				{
					$indexes[] = $matches['1'][$i];
				}
			}

			$is_array = TRUE;
		}
		else
		{
			$indexes	= array();
			$is_array	= FALSE;
		}

		$this->_field_data[$field_name] = array(
			'field'				=> $field_name,
			'label'				=> $field_display,
			'rules'				=> $valid_rules,
			'is_array'			=> $is_array,
			'keys'				=> $indexes,
			'postdata'			=> NULL,
			'error'				=> ''
		);

		return $this;

	}

	public function run() {
		if (count($_POST) == 0)
		{
			return FALSE;
		}

		if (count($this->_field_data) == 0)
		{
			return FALSE;
		}

		foreach ($this->_field_data as $field => $row)
		{
			if ($row['is_array'] == TRUE)
			{
				$this->_field_data[$field]['postdata'] = $this->_reduce_array($_POST, $row['keys']);
			}
			else
			{
				if (isset($_POST[$field]) AND $_POST[$field] != "")
				{
					$this->_field_data[$field]['postdata'] = $_POST[$field];
				}
			}

			$this->_execute($row, explode('|', $row['rules']), $this->_field_data[$field]['postdata']);
		}

		$total_errors = count($this->_error_array);

		$this->_reset_post_array();

		if ($total_errors == 0)
		{
			$this->return_status = TRUE;
		} else {
			$this->return_status = FALSE;	
		}

		return $this;
	}

	protected function _reduce_array($array, $keys, $i = 0)
	{
		if (is_array($array))
		{
			if (isset($keys[$i]))
			{
				if (isset($array[$keys[$i]]))
				{
					$array = $this->_reduce_array($array[$keys[$i]], $keys, ($i+1));
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return $array;
			}
		}

		return $array;
	}

	protected function _reset_post_array()
	{
		foreach ($this->_field_data as $field => $row)
		{
			if ( ! is_null($row['postdata']))
			{
				if ($row['is_array'] == FALSE)
				{
					if (isset($_POST[$row['field']]))
					{
						$_POST[$row['field']] = $this->prep_for_form($row['postdata']);
					}
				}
				else
				{
					$post_ref =& $_POST;

					if (count($row['keys']) == 1)
					{
						$post_ref =& $post_ref[current($row['keys'])];
					}
					else
					{
						foreach ($row['keys'] as $val)
						{
							$post_ref =& $post_ref[$val];
						}
					}

					if (is_array($row['postdata']))
					{
						$array = array();
						foreach ($row['postdata'] as $k => $v)
						{
							$array[$k] = $this->prep_for_form($v);
						}

						$post_ref = $array;
					}
					else
					{
						$post_ref = $this->prep_for_form($row['postdata']);
					}
				}
			}
		}
	}

	public function prep_for_form($data = '')
	{
		if (is_array($data))
		{
			foreach ($data as $key => $val)
			{
				$data[$key] = $this->prep_for_form($val);
			}

			return $data;
		}

		if ($this->_safe_form_data == FALSE OR $data === '')
		{
			return $data;
		}

		return str_replace(array("'", '"', '<', '>'), array("&#39;", "&quot;", '&lt;', '&gt;'), stripslashes($data));
	}


	protected function _execute($row, $rules, $postdata = NULL, $cycles = 0){
		global $lang;

		if (is_array($postdata))
		{
			foreach ($postdata as $key => $val)
			{
				$this->_execute($row, $rules, $val, $cycles);
				$cycles++;
			}

			return;
		}

		$callback = FALSE;
		if ( ! in_array('required', $rules) AND is_null($postdata))
		{
			if (preg_match("/(callback_\w+(\[.*?\])?)/", implode(' ', $rules), $match))
			{
				$callback = TRUE;
				$rules = (array('1' => $match[1]));
			}
			else
			{
				return;
			}
		}

		foreach ($rules As $rule)
		{
			$_in_array = FALSE;

			if ($row['is_array'] == TRUE AND is_array($this->_field_data[$row['field']]['postdata']))
			{
				if ( ! isset($this->_field_data[$row['field']]['postdata'][$cycles]))
				{
					continue;
				}

				$postdata = $this->_field_data[$row['field']]['postdata'][$cycles];
				$_in_array = TRUE;
			}
			else
			{
				$postdata = $this->_field_data[$row['field']]['postdata'];
			}
			
			$param = FALSE;
			if (preg_match("/(.*?)\[(.*)\]/", $rule, $match))
			{
				$rule	= $match[1];
				$param	= $match[2];
			}

			
			if ( ! method_exists($this, $rule))
			{
				if (function_exists($rule))
				{
					$result = $rule($postdata);

					if ($_in_array == TRUE)
					{
						$this->_field_data[$row['field']]['postdata'][$cycles] = (is_bool($result)) ? $postdata : $result;
					}
					else
					{
						$this->_field_data[$row['field']]['postdata'] = (is_bool($result)) ? $postdata : $result;
					}
				}
				else
				{
					echo $rule." not exists.";
				}

				continue;
			}

			$result = $this->$rule($postdata, $param);

			if ($_in_array == TRUE)
			{
				$this->_field_data[$row['field']]['postdata'][$cycles] = (is_bool($result)) ? $postdata : $result;
			}
			else
			{
				$this->_field_data[$row['field']]['postdata'] = (is_bool($result)) ? $postdata : $result;
			}
			
			if ($result === FALSE)
			{
				if($rule != "callback"){
					if ( ! isset($lang[$rule]))
					{
						$line = 'Unable to access an error message corresponding to your field name.';
					}
					else
					{
						$line = $lang[$rule];
					}
					$message = sprintf($line, $row['label'], $param);
				} else {
					$line = "Unable to find callback validation rule: %s";
					$message = sprintf($line, $param);
				}

				$this->_field_data[$row['field']]['error'] = $message;

				if ( ! isset($this->_error_array[$row['field']]))
				{
					$this->_error_array[$row['field']] = $message;
				}

				return;
			}
		}
		
	}

	public function callback($str,$value)
	{
		if ( ! is_array($str))
		{
			if(function_exists($value)) {
				$value($str);
				return TRUE;
			} else {
				return FALSE;
			}
		}
		else
		{
			return ( ! empty($str));
		}
	}

	public function required($str)
	{
		if ( ! is_array($str))
		{
			return (trim($str) == '') ? FALSE : TRUE;
		}
		else
		{
			return ( ! empty($str));
		}
	}

	public function max_length($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(strlen($str) > $value){
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function min_length($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(strlen($str) < $value){
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function valid_email($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(filter_var($str, FILTER_VALIDATE_EMAIL)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function exact_length($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(strlen($str) == $value){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function alpha($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(ctype_alpha($str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function alpha_numeric($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(ctype_alnum((string) $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function alpha_numeric_spaces($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(preg_match('/^[A-Z0-9 ]+$/i', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function alpha_dash($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(preg_match('/^[a-z0-9_-]+$/i', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function numeric($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function is_integer($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(preg_match('/^[\-+]?[0-9]+$/', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function regex_match($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(preg_match('/'.$value.'/', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function matches($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(isset($str, $_REQUEST[$value]) && $str === $_REQUEST[$value]){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function differs($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(!(isset($_POST[$value]) && $_POST[$value] === $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function is_natural($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(ctype_digit((string) $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function is_natural_no_zero($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(ctype_digit((string) $str) && $str != 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function decimal($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(preg_match('/^[\-+]?[0-9]+\.[0-9]+$/', $str)){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function less_than($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(is_numeric($str) || $str < $value){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function less_than_equal_to($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(is_numeric($str) || $str <= $value){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function greater_than($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(is_numeric($str) || $str > $value){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function greater_than_equal_to($str,$value){
		if(empty($str) || empty($value)){
			return FALSE;
		} else {
			if(is_numeric($str) || $str >= $value){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function valid_base64($str){
		if(empty($str)){
			return FALSE;
		} else {
			if(base64_encode(base64_decode($str)) === $str){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	private function valid_url($str) {
	  	if (empty($str))
            {
                return FALSE;
            }
            elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches))
            {
                    if (empty($matches[2]))
                    {
                            return FALSE;
                    }
                    elseif ( ! in_array($matches[1], array('http', 'https'), TRUE))
                    {
                            return FALSE;
                    }

                    $str = $matches[2];
            }

            $str = 'http://'.$str;
            if (version_compare(PHP_VERSION, '5.2.13', '==') === 0 OR version_compare(PHP_VERSION, '5.3.2', '==') === 0)
            {
                    sscanf($str, 'http://%[^/]', $host);
                    $str = substr_replace($str, strtr($host, array('_' => '-', '-' => '_')), 7, strlen($host));
            }
            return (filter_var($str, FILTER_VALIDATE_URL) !== FALSE);
	}

	private function valid_ip($ip, $which = ''){
        switch (strtolower($which))
        {
            case 'ipv4':
                $which = FILTER_FLAG_IPV4;
                break;
            case 'ipv6':
                $which = FILTER_FLAG_IPV6;
                break;
            default:
                $which = NULL;
				break;
        }

        return (bool) filter_var($ip, FILTER_VALIDATE_IP, $which);
    }

	public function set_value($field = '', $default = '') {
		if ( ! isset($this->_field_data[$field]))
		{
			return $default;
		}

		if (is_array($this->_field_data[$field]['postdata']))
		{
			return array_shift($this->_field_data[$field]['postdata']);
		}

		return $this->_field_data[$field]['postdata'];
	}

	public function set_error_message($field, $message, $param = '') {
		$message = sprintf($message, $this->_field_data[$field]['label'], $param);

		$message = $this->error_wrapper_open.$message.$this->error_wrapper_close;
		
		$this->_field_data[$field]['error'] = $message;

		if ( ! isset($this->_error_array[$this->_field_data[$field]['field']]))
		{
			$this->_error_array[$this->_field_data[$field]['field']] = $message;
		}
	}

	public function get_error($field) {
		if ( ! isset($this->_field_data[$field]['error']) OR $this->_field_data[$field]['error'] == '')
		{
			return '';
		}
		return $this->error_wrapper_open.$this->_field_data[$field]['error'].$this->error_wrapper_close;
	}

}

?>