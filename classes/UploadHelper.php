<?php

class UploadHelper{
	private $file_args;
	public $config = array();
	private $upload_path = false;
	private $allowed_types = false;
	private $max_size = false;
	private $file_name = false;
	private $overwrite = true;
	private $encrypt_name = false;
	private $remove_spaces = true;
	private $returntype = "array";

	private $filename;
	private $filetype;
	private $file_tmp_name;
	private $file_size;

	private $errormsg = "";
	public $success = false;
	private $allok = false;
	private $data;

	public function __construct() {
		
	}

	public function Upload($file = null) {
		$this->file_args = @$_FILES[$file];

		$this->filename = $this->file_args['name'];
		$this->filetype = $this->file_args['type'];
		$this->file_tmp_name = $this->file_args['tmp_name'];
		$this->file_size = $this->file_args['size'];

		if(is_array($this->config) && sizeof($this->config) > 0) {

			if(!isset($this->config['upload_path'])) {
				$this->errormsg = "Uploader require 'upload_path' declared or setup";
				return;
			}

			$this->upload_path = $this->config['upload_path'];

			if(isset($this->config['allowed_types'])) {
				$this->allowed_types = $this->config['allowed_types'];
			}

			if(isset($this->config['max_size'])) {
				$this->max_size = (int) $this->config['max_size'];
			}

			if(isset($this->config['file_name'])) {
				$this->file_name = $this->config['file_name'];
			}

			if(isset($this->config['overwrite'])) {
				$this->overwrite = $this->config['overwrite'];
			}

			if(isset($this->config['encrypt_name'])) {
				$this->encrypt_name = $this->config['encrypt_name'];
			}

			if(isset($this->config['remove_spaces'])) {
				$this->remove_spaces = $this->config['remove_spaces'];
			}

			if(isset($this->config['return_type'])) {
				$this->returntype = $this->config['return_type'];
			}

		} else {
			$this->errormsg = "Please declare or setup the config variable for file upload";
			return;
		}
		return $this;
	}

	public function progressUpload() {

		if(!$this->isUploadPathExists()) {
			$this->errormsg = "Upload path/folder not found";
			return $this;
		}

		if(!$this->isUploadPathWritetable()) {
			$this->errormsg = "Upload path/folder is not writable";
			return $this;
		}

		if(strlen($this->allowed_types) > 0) {
			$allowed_types_explode = explode("|", $this->allowed_types);
			if(sizeof($allowed_types_explode) > 0) {
				$get_last_extension = $this->getExtension($this->filename);
				if(!in_array($get_last_extension, $allowed_types_explode)) {
					$this->errormsg = "File extension not allowed, only file with ".implode(",", $allowed_types_explode)." extension allowed to upload";
					return $this;
				}
			}

		}

		if(strlen($this->max_size) > 0 && is_integer($this->max_size)) {
			if($this->file_size > $this->max_size) {
				$this->errormsg = "Allowed maximum file size is ".$this->max_size." kilobytes";
				return $this;
			}
		} 
		$this->writeTheFile();

		return $this;

	}

	private function writeTheFile() {
			if($this->remove_spaces) {
				$this->filename = @preg_replace('/\s+/', '', $this->filename);
			}

			if(strlen($this->file_name) > 0) {
				$this->filename = $this->file_name;
			}

			if($this->encrypt_name) {
				$this->filename = $this->generateRandomName();
			}

			if(!$this->overwrite) {
				$this->filename = $this->file_newname($this->config['upload_path'], $this->filename);
			}

			if(@move_uploaded_file($this->file_tmp_name, $this->config['upload_path'].'/'.$this->filename)) {
				$this->success = true;
				$rawname = explode(".", $this->filename);
				$ext = "";
				switch($this->returntype){
					case "array":
						$this->data = array(
							'file_name' => $this->filename,
							'file_type' => $this->filetype,
							'file_path' => $this->config['upload_path'],
							'full_path' => $this->config['upload_path'].'/'.$this->filename,
							'raw_name' => $rawname[0],
							'orig_name' => $this->file_args['name'],
							'file_ext' => $this->getExtension($this->filename),
							'file_size' => $this->file_size,
						);
					break;
					case "json":
						$temp = array(
							'file_name' => $this->filename,
							'file_type' => $this->filetype,
							'file_path' => $this->config['upload_path'],
							'full_path' => $this->config['upload_path'].'/'.$this->filename,
							'raw_name' => $rawname[0],
							'orig_name' => $this->file_args['name'],
							'file_ext' => $this->getExtension($this->filename),
							'file_size' => $this->file_size,
						);
						$this->data = json_encode($temp);
					break;
					case "xml":

					break;
				}
				

			} else {
				$this->errormsg = "Upload the file failed, please try again";
				return $this;
			}
	}

	private function getExtension($string) {
		$temp = explode(".", $string);
		$ext = end($temp);
		return $ext;
	}

	public function data() {
		return $this->data;
	}

	private function file_newname($path, $filename){
		$ext = "";
	    if ($pos = strrpos($filename, '.')) {
	           $name = substr($filename, 0, $pos);
	           $ext = substr($filename, $pos);
	    } else {
	           $name = $filename;
	    }

	    $newpath = $path.'/'.$filename;
	    $newname = $filename;
	    $counter = 0;
	    while (file_exists($newpath)) {
	           $newname = $name .'_'. $counter . $ext;
	           $newpath = $path.'/'.$newname;
	           $counter++;
	     }

	    return $newname;
	}

	public function getError() {
		return $this->errormsg;
	}

	private function generateRandomName() {
		return $this->file_newname($this->config['upload_path'], md5($this->filename.time().time().time().rand(0, 9).rand(0, 9).rand(0,9)));
	}

	private function isUploadPathWritetable() {
		return @is_writable($this->upload_path);
	}

	private function isUploadPathExists() {
		if(strlen($this->upload_path) > 1) {
			if(@is_dir($this->upload_path)) {
				return true;
			}
			return false;
		}
		return false;
	}
}

?>