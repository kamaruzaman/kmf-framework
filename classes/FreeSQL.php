<?php

class FreeSQL{


	private $db_hostname, $db_username, $db_password, $db_name;
	public $resource;


	public function __construct($args = array()) {
		$this->db_hostname = $args['db_hostname'];
		$this->db_username = $args['db_username'];
		$this->db_password = $args['db_password'];
		$this->db_name = $args['db_name'];
		$this->connect_db();
		$this->select_db();

	}

	public function connect_db() {
		$this->resource = @mysql_connect($this->db_hostname, $this->db_username, $this->db_password) or die("Connect to the mysql server failed: ".mysql_error());
	}

	public function select_db() {
		return @mysql_select_db($this->db_name) or die("Select database failed: ".mysql_error());
	}

	public function query($query) {
		$res = mysql_query($query) or die("Query failed: ".mysql_error());
		return $res;
	}

	public function fetch_object($resource_sql) {
		return @mysql_fetch_object($resource_sql);
	}

	public function fetch_array($resource_sql) {
		return @mysql_fetch_array($resource_sql);
	}

	public function num_rows($resource_sql) {
		return @mysql_num_rows($resource_sql);
	}

	public function lastInsertID(){
		return @mysql_insert_id();
	}

	public function safe_sql($string) {
		return htmlspecialchars($string, ENT_QUOTES);
	}

}

?>