<?php

class BootTheme{

	private $css_path = array();
	private $js_path = array();
	private $return = "";
	private $css = "";
	private $script = "";
	private $htmltitle = "";


	public function __construct($args) {
		$this->css_path = $args['css_path'];

		if(isset($args['js_path'])) {
			$this->js_path = $args['js_path'];
		}
	}

	public function add_css($path) {
		return "<link rel=\"stylesheet\" href=\"".$path."\">\r\n";
	}

	public function add_js($path) {
		return "<script src=\"".$path."\"></script>\r\n";
	}

	public function load_css() {
		return $this->build_css_content();
	}

	public function load_js() {
		return $this->build_js_content();
	}

	public function htmltitle($string){
		$this->htmltitle = $string;
	}

	private function html(){
		$temp = "";
		$temp .= "<!DOCTYPE html>\r\n";
		$temp .= "<html>\r\n";
		$temp .= "<head>\r\n";
		$temp .= "<title>{$this->htmltitle}</title>\r\n";
		$temp .= $this->load_css();
		if(!empty($this->css)){
			$temp .= "<style>\r\n";
			$temp .= $this->css;
			$temp .= "</style>\r\n";
		}
		$temp .= "</head>\r\n";
		$temp .= "<body>";
		return $temp;
	}

	public function navbar($args){
		$class = array_key_exists('class', $args) ? "navbar navbar-default ".$args['class'] : "navbar navbar-default";
		$id = array_key_exists('id', $args) ? $args['id'] : "";
		$brand = array_key_exists('brand', $args) ? !empty($args['brand'])? $args['brand'] : "" : "";
		$brandlink = array_key_exists('brand_link', $args) ? !empty($args['brand_link'])? $args['brand_link'] : "#" : "#";
		$name = array_key_exists('name', $args) ? !empty($args['name'])? $args['name'] : "navbar-minimize" : "navbar-minimize";

		if(preg_match('/navbar-fixed-top/',$class)){
			$this->css .= "body{ padding-top: 70px; }\r\n";
		}

		$this->return .= "<nav class='{$class}'";
		$this->return .= $id != "" ? " id='{$id}'" : "";
		$this->return .= " role='navigation'>\r\n";

		$this->return .= "<div class='navbar-header'>\r\n";
    	$this->return .= "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.{$name}'>\r\n";
      	$this->return .= "<span class='sr-only'>Toggle navigation</span>\r\n";
      	$this->return .= "<span class='icon-bar'></span>\r\n";
      	$this->return .= "<span class='icon-bar'></span>\r\n";
      	$this->return .= "<span class='icon-bar'></span>\r\n";
    	$this->return .= "</button>\r\n";
    	$this->return .= "<a class='navbar-brand' href='{$brandlink}'>{$brand}</a>\r\n";
  		$this->return .= "</div>\r\n";

  		$this->return .= "<div class='collapse navbar-collapse {$name}'>\r\n";
  		$this->return .= "<ul class='nav navbar-nav'>\r\n";

  		if(array_key_exists('navs', $args) && is_array($args['navs'])){
  			foreach($args['navs'] as $nav){
  				if(array_key_exists('dropdown', $nav) && is_array($nav['dropdown'])){
  					$title = array_key_exists('title', $nav) ? !empty($nav['title'])? $nav['title'] : "" : "";
  					$class = array_key_exists('class', $nav) ? !empty($nav['class'])? $nav['class'] : "" : "";
  					$this->return .= "<li class='dropdown";
  					$this->return .= $class != "" ? " {$class}" : "";
  					$this->return .= "'>\r\n";
        			$this->return .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>\r\n{$title} <b class='caret'>\r\n</b></a>\r\n";
        			$this->return .= "<ul class='dropdown-menu'>\r\n";
        			foreach($nav['dropdown'] as $list){
        				if(array_key_exists('divider', $list) && $list['divider'] == true){
        					$this->return .= "<li class='divider'></li>\r\n";
        				} else if(array_key_exists('header', $list) && $list['header'] == true){
        					$title = array_key_exists('title', $list) ? !empty($list['title'])? $list['title'] : "" : "";
		  					$this->return .= "<li class='dropdown-header'>{$title}</li>\r\n";
        				} else {
        					$title = array_key_exists('title', $list) ? !empty($list['title'])? $list['title'] : "" : "";
		  					$link = array_key_exists('link', $list) ? !empty($list['link'])? $list['link'] : "#" : "#";
		  					$this->return .= "<li><a href='{$link}'>{$title}</a></li>\r\n";
        				}
        			}
  					$this->return .= "</ul>\r\n";
      				$this->return .= "</li>\r\n";
  				} else {
  					$title = array_key_exists('title', $nav) ? !empty($nav['title'])? $nav['title'] : "" : "";
  					$link = array_key_exists('link', $nav) ? !empty($nav['link'])? $nav['link'] : "#" : "#";
  					$class = array_key_exists('class', $nav) ? !empty($nav['class'])? $nav['class'] : "" : "";
  					$this->return .= "<li";
  					$this->return .= $class != "" ? " class='{$class}'" : "";
  					$this->return .= "><a href='{$link}'>{$title}</a></li>\r\n";
  				}
  			}
  		}

  		$this->return .= "</ul>\r\n";
  		$this->return .= "</div>\r\n";

		$this->return .= "</nav>\r\n";
	}

	public function responsive_images($args = array()) {
		$alt = "";
		$width = "";
		$height = "";
		if(sizeof($args) > 0) {
			if(isset($args['alt'])) { $alt =  "alt='".$args['alt']."' "; }
			if(isset($args['width'])) { $width = "width='".$args['width']."' "; }
			if(isset($args['height'])) { $height = "height='".$args['height']."' "; }
			$this->return .= "<img src='".$args['src']."' class='img-responsive' ".$alt."".$width."".$height."/>\r\n";
		}
		return $this;
	}

	public function headings($size, $string,$callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$value);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<".$size.">".$string."</".$size.">\r\n";
		} else {
			$this->return .= "<".$size.">".$string."</".$size.">\r\n";
		}
		return $this;
	}

	public function _echo($string, $callback =''){
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$value);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= $string;
		} else {
			$this->return .= $string;
		}
		return $this;
	}

	public function br(){
		$this->return .= "<br/>";
	}

	public function p($args) {
		if(is_array($args) && sizeof($args) > 0) {
			$class = "";
			if(isset($args['class'])) { $class =  "class='".$args['class']."' "; }
			if(preg_match_all('/\{(.+?)\}/', $args['string'],$matches)){
				$i = 0;
				foreach($matches['1'] as $match){
					if(isset($this->$match) && $this->$match != ""){
						$args['string'] = preg_replace('/\{'.$match.'\}/',$this->$match,$args['string']);
					}
					$i++;
				}
			}
			if(array_key_exists('callback', $args) && !empty($args['callback'])){
				$this->$args['callback'] = "";
				$this->$args['callback'] .= "<p ".$class.">".$args['string']."</p>";
			} else {
				$this->return .= "<p ".$class.">\r\n".$args['string']."</p>";	
			}
		} else {
			if(preg_match_all('/\{(.+?)\}/', $args,$matches)){
				$i = 0;
				foreach($matches['1'] as $match){
					if(isset($this->$match) && $this->$match != ""){
						$args = preg_replace('/\{'.$match.'\}/',$this->$match,$args);
					}
					$i++;
				}
			}
			$this->return .= "<p>\r\n".$args."</p>\r\n";
		}
		return $this;
	}

	public function small_text($string,$callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<small>".$string."</small>\r\n";
		} else {
			$this->return .= "<small>".$string."</small>\r\n";
		}
		return $this;
	}

	public function strong($string,$callback) {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<strong>".$string."</strong>\r\n";
		} else {
			$this->return .= "<strong>".$string."</strong>\r\n";
		}
		return $this;
	}

	public function address($string,$callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<address>".$string."</address>\r\n";
		} else {
			$this->return .= "<address>".$string."</address>\r\n";
		}
		return $this;
	}

	public function blockquote($args) {
		if(is_array($args) && sizeof($args) > 0) {
			$class = "";
			if(isset($args['class'])) { $class =  "class='".$args['class']."' "; }
			if(preg_match_all('/\{(.+?)\}/',$args['string'],$matches)){
				$i = 0;
				foreach($matches['1'] as $match){
					if(isset($this->$match) && $this->$match != ""){
						$args['string'] = preg_replace('/\{'.$match.'\}/',$this->$match,$args['string']);
					}
					$i++;
				}
			}
			if(array_key_exists('callback', $args) && !empty($args['callback'])){
				$this->$args['callback'] = "";
				$this->$args['callback'] .= "<blockquote ".$class.">".$args['string']."</blockquote>";
			} else {
				$this->return .= "<blockquote ".$class.">".$args['string']."</blockquote>";
			}
		} else {
			if(preg_match_all('/\{(.+?)\}/', $args,$matches)){
				$i = 0;
				foreach($matches['1'] as $match){
					if(isset($this->$match) && $this->$match != ""){
						$args = preg_replace('/\{'.$match.'\}/',$this->$match,$args);
					}
					$i++;
				}
			}
			$this->return .= "<blockquote>".$args."</blockquote>\r\n";
		}
		return $this;
	}

	public function list_unordered($args) {
		if(is_array($args) && sizeof($args) > 0) {
			if(isset($args['listitem']) && is_array($args['listitem'])) {
				$listitem = "";
				$class = "";
				if(isset($args['class'])) { $class =  "class='".$args['class']."' "; }
				foreach($args['listitem'] as $item) {
					$listitem .= "<li>".$item."</li>\r\n";
				}
				$this->return .= "<ul ".$class.">".$listitem."</ul>";
			}
			return $this;
		} 
	}


	public function list_ordered($args) {
		if(is_array($args) && sizeof($args) > 0) {
			if(isset($args['listitem']) && is_array($args['listitem'])) {
				$listitem = "";
				$class = "";
				if(isset($args['class'])) { $class =  "class='".$args['class']."' "; }
				foreach($args['listitem'] as $item) {
					$listitem .= "<li>".$item."</li>\r\n";
				}
				$this->return .= "<ol ".$class.">".$listitem."</ol>";
				return $this;
			}

		}
	}

	public function pre($string, $class = '', $callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<pre class='".$class."'>{$string}</pre>\r\n";
		} else {
			$this->return .= "<pre class='".$class."'>{$string}</pre>\r\n";	
		}
		return $this;
	}

	public function code($string, $class = '', $callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<code class='".$class."'>{$string}</code>\r\n";
		} else {
			$this->return .= "<code class='".$class."'>{$string}</code>\r\n";	
		}
		return $this;
	}

	public function span($string, $class = '', $callback = '') {
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		if($callback != ''){
			$this->$callback = "";
			$this->$callback .= "<span class='".$class."'>{$string}</span>\r\n";
		} else {
			$this->return .= "<span class='".$class."'>{$string}</span>\r\n";	
		}
		return $this;
	}

	public function tables($args) {
		if(is_array($args) && sizeof($args) > 0) {
			$class = "class='table'";
			$id = "";
			$width = "";
			$cellspacing = "";
			$cellpadding = "";
			$border = "";
			$align = "";
			if(isset($args['class']))  { $class = "class='".$args['class']."' "; }
			if(isset($args['id']))  { $id = "id='".$args['id']."' "; }
			if(isset($args['width']))  { $width = "width='".$args['width']."' "; }
			if(isset($args['cellspacing']))  { $cellspacing = "cellspacing='".$args['cellspacing']."' "; }
			if(isset($args['cellpadding']))  { $cellpadding = "cellpadding='".$args['cellpadding']."' "; }
			if(isset($args['border']))  { $border = "border='".$args['border']."' "; }
			if(isset($args['align']))  { $align = "align='".$args['align']."' "; }

			if(isset($args['heading']) && is_array($args['heading']) && sizeof($args['heading']) > 0) {
				if(isset($args['rowsdata']) && is_array($args['rowsdata']) && sizeof($args['rowsdata']) > 0) {
					$this->return .= "<table ".$class." ".$id."".$width."".$cellpadding."".$cellspacing."".$border."".$align.">\r\n";
					$this->return .= "<thead>\r\n";
					for($a = 0; $a < sizeof($args['heading']); $a++) {
						$this->return .= "<th>".$args['heading'][$a]."</th>\r\n";
					}
					$this->return .="</thead>\r\n";
					$this->return .= "<tbody>\r\n";
					if(sizeof($args['rowsdata']) > 0) {
						for($i = 0; $i < sizeof($args['rowsdata']); $i++)
						{	$this->return .= "<tr>\r\n";
							for($b = 0; $b < sizeof($args['rowsdata'][$i]); $b++) {
								if(isset($args['set_empty'])) {
									if(empty($args['rowsdata'][$i][$b])) {
										$args['rowsdata'][$i][$b] = $args['set_empty'];
									}
								} 
								if(isset($args['function'])) {
									$this->return .= "<td>".call_user_func($args['function'], $args['rowsdata'][$i][$b])."</td>\r\n"; 
								} else {
									$this->return .= "<td>".$args['rowsdata'][$i][$b]."</td>\r\n"; 
								}
							}
							$this->return .= "</tr>\r\n";
						}
					} else {
						$this->return .= "<tr><td colspan='".sizeof($args['heading'])."'>No data found</td></tr>";
					}
					$this->return .= "</tbody>\r\n";
					$this->return .= "</table>\r\n";
				}

				if(is_resource($args['rowsdata']) && class_exists('FreeSQL')) {
					$sql_count = @mysql_num_rows($args['rowsdata']);
					if($sql_count > 0) {

						$this->return .= "<table ".$class." ".$id."".$width."".$cellpadding."".$cellspacing."".$border."".$align.">\r\n";
						$this->return .= "<thead>\r\n";
						for($a = 0; $a < sizeof($args['heading']); $a++) {
							$this->return .= "<th>".$args['heading'][$a]."</th>\r\n";
						}
						$this->return .="</thead>\r\n";
						$this->return .= "<tbody>\r\n";

						while($rows = @mysql_fetch_array($args['rowsdata'])) {
							$this->return .= "<tr>";
							for($i = 0; $i < sizeof($rows)/2; $i++) {
								if(isset($args['set_empty'])) {
									if(empty($rows[$i])) {
										$rows[$i] = $args['set_empty'];
									}
								} 

								if(isset($args['function'])) {
									$this->return .= "<td>".call_user_func($args['function'], $rows[$i])."</td>\r\n";
								} else {
									$this->return .= "<td>".$rows[$i]."</td>\r\n";
								}
							}
							$this->return .= "</tr>";
						}

						$this->return .= "</tbody>\r\n";
						$this->return .= "</table>\r\n";
					} else {
						$this->return .= "<table ".$class." ".$id."".$width."".$cellpadding."".$cellspacing."".$border."".$align.">\r\n";
						$this->return .= "<thead>\r\n";
						for($a = 0; $a < sizeof($args['heading']); $a++) {
							$this->return .= "<th>".$args['heading'][$a]."</th>\r\n";
						}
						$this->return .="</thead>\r\n";
						$this->return .= "<tbody>\r\n";
						$this->return .= "<tr><td colspan='".sizeof($args['heading'])."'>No data found</td></tr>";
						$this->return .= "</tbody>\r\n";
						$this->return .= "</table>\r\n";
					}

				}
			}
			return $this;
		}
	}

	public function form_open($args = array()){
		$action = "";
		$class = "";
		$file = "";
		$action = array_key_exists('action',$args)? $args['action'] == "" ? "" : $args['action'] : "";
		$class = isset($args['class'])? $args['class'] : "";
		$file = isset($args['enctype']) && $args['enctype'] == true ? "multipart/form-data" : "";
		if($file == ""){
			$this->return .= "<form action='{$action}' class='{$class}' method='post'>\r\n";	
		} else {
			$this->return .= "<form action='{$action}' class='{$class}' method='post' enctype='{$file}'>\r\n";
		}
		return $this;
	}

	public function form_close(){
		$this->return .= "</form>\r\n";
		return $this;
	}

	public function input($args = array()){
		global $kmf;
		$class = "";
		$id = "";
		$type = "";
		$placeholder = "";
		$value = "";
		$name = "";
		$class = isset($args['class'])? "form-control ".$args['class'] : "form-control";
		$typelists = array('text', 'password', 'datetime', 'datetime-local', 'date', 'month', 'time', 'week', 'number', 'email', 'url', 'search', 'tel', 'color');
		$type = array_key_exists('type',$args)? in_array($args['type'],$typelists)? $args['type'] : "text" : "text";
		$placeholder = array_key_exists('placeholder', $args) ? $args['placeholder'] : "";
		$value = array_key_exists('value', $args) ? $args['value'] : "";
		$name = array_key_exists('name', $args) ? $args['name'] : "";
		$value = array_key_exists('Validation', $kmf)? $kmf->Validation->set_value($name) != ''? $kmf->Validation->set_value($name) : $value : $value ; 
		$id = array_key_exists('id', $args) ? $args['id'] : "";
		$this->return .= "<input type='{$type}' class='{$class}'";
		$placeholder == "" ? "" : $this->return .= " placeholder='{$placeholder}'";
		$value == "" ? "" : $this->return .= " value='{$value}'";

		$name == "" ? "" : $this->return .= " name='{$name}'";
		$id == "" ? "" : $this->return .= " id='{$id}'";
		$this->return .= "/>\r\n";
		if(array_key_exists('Validation', $kmf)){
			if($kmf->Validation->get_error($name) != null){
				$error = $kmf->Validation->get_error($name);
				$this->return .= $error;
			}	
		}
		return $this;
	}

	public function fileinput($args){
		$class = "";
		$id = "";
		$name = "";
		$class = isset($args['class'])? $args['class'] : "";
		$name = array_key_exists('name', $args) ? $args['name'] : "fileinput";
		if(preg_match('/(.+)\[\]/',$name,$matches)){
			$id = array_key_exists('id', $args) ? $args['id'] : $matches[1];
		} else {
			$id = array_key_exists('id', $args) ? $args['id'] : $name;
		}
		if(array_key_exists('multiple', $args) && $args['multiple'] == true){
			$this->return .= "<input multiple='' type='file' id='{$id}' name='{$name}'";
			$this->return .= $class != ""? " class='{$class}'" : "";
			$this->return .= "/>\r\n";	
		} else {
			$this->return .= "<input type='file' id='{$id}' name='{$name}'";
			$this->return .= $class != ""? " class='{$class}'" : "";
			$this->return .= "/>\r\n";	
		}
		
		if(array_key_exists('style', $args) && $args['style'] == "well"){
			$this->script .= "$('#{$id}').kmf_file_input({";
			$this->script .= "\r\nstyle:'well',";
			$this->script .= array_key_exists('no_icon', $args)? "\r\nno_icon:'".$args['no_icon']."'," : "\r\nno_icon:'fa fa-cloud-upload',"; 
			$this->script .= array_key_exists('btn_choose', $args)? "\r\nbtn_choose:'".$args['btn_choose']."'," : "btn_choose:'Drop files here or click to choose',"; 
			$this->script .= array_key_exists('btn_change', $args)? "\r\nbtn_change:'".$args['btn_change']."'," : "btn_change:null,";
			$this->script .= "\r\nthumbnail:'large',";
			$this->script .= array_key_exists('droppable', $args)? "\r\ndroppable:'".$args['droppable']."'\r\n" : "\r\ndroppable:false\r\n";
			$this->script .= "});\r\n";	
		} else {
			$this->script .= "$('#{$id}').kmf_file_input({";
			$this->script .= array_key_exists('no_file', $args)? "\r\nno_file:'".$args['no_file']."'," : ""; 
			$this->script .= array_key_exists('btn_choose', $args)? "\r\nbtn_choose:'".$args['btn_choose']."'," : ""; 
			$this->script .= array_key_exists('btn_change', $args)? "\r\nbtn_change:'".$args['btn_change']."'," : "";
			$this->script .= array_key_exists('droppable', $args)? "\r\ndroppable:'".$args['droppable']."'\r\n" : "\r\ndroppable:false\r\n";
			$this->script .= "});\r\n";	
		}
		return $this;
	}

	public function textarea($args = array()){
		$class = "";
		$id = "";
		$name = "";
		$rows = "";
		$cols = "";
		$value = "";
		$class = isset($args['class'])? "form-control ".$args['class'] : "form-control";
		$id = array_key_exists('id', $args) ? $args['id'] : "";
		$name = array_key_exists('name', $args) ? $args['name'] : "";
		$rows = array_key_exists('rows', $args) ? $args['rows'] : "";
		$cols = array_key_exists('cols', $args) ? $args['cols'] : "";
		$value = array_key_exists('value', $args) ? $args['value'] : "";
		$this->return .= "<textarea class='{$class}'";
		$rows == "" ? "" : $this->return .= " rows='{$rows}'";
		$cols == "" ? "" : $this->return .= " cols='{$cols}'";
		$name == "" ? "" : $this->return .= " name='{$name}'";
		$id == "" ? "" : $this->return .= " id='{$id}'";
		$this->return .= ">";
		$value == "" ? "" : $this->return .= "$value";
		$this->return .= "</textarea>\r\n";
		return $this;
	}

	public function radio($args = array()){
		$class = "";
		$labelclass = "";
		$id = "";
		$name = "";
		$label = "";
		$value = "";
		$checked = "";
		$class = isset($args['class'])? "radio ".$args['class'] : "radio";
		$labelclass = array_key_exists('labelclass',$args)? $args['labelclass'] : "";
		$id = array_key_exists('id', $args) ? $args['id'] : "";
		$name = array_key_exists('name', $args) ? $args['name'] : "";
		$value = array_key_exists('value', $args) ? $args['value'] : "";
		$label = array_key_exists('label', $args) ? $args['label'] : "Label not set";
		$checked = array_key_exists('checked', $args) ? $args['checked'] == true ? "checked" : ""  : "";
		//$return .= "<div class='{$class}'>\r\n";
		$this->return .= "<label";
		$labelclass == "" ? "" : $this->return .= " class='{$labelclass}'";
		$this->return .= ">\r\n";
		$this->return .= "<input type='radio'";
		$name == "" ? "" : $this->return .= " name='{$name}'";
		$id == "" ? "" : $this->return .= " id='{$id}'";
		$value == "" ? "" : $this->return .= " value='{$value}'";
		$checked != "" ? $this->return .= " checked='checked'" : "";
		$this->return .= "/>\r\n";
		$this->return .= "{$label}\r\n";
		$this->return .= "</label>\r\n";
		//$return .= "</div>\r\n";
		return $this;
	}

	public function checkbox($args = array()){
		$class = "";
		$labelclass = "";
		$id = "";
		$name = "";
		$label = "";
		$value = "";
		$checked = "";
		$class = isset($args['class'])? "checkbox ".$args['class'] : "checkbox";
		$labelclass = array_key_exists('labelclass',$args)? $args['labelclass'] : "";
		$id = array_key_exists('id', $args) ? $args['id'] : "";
		$name = array_key_exists('name', $args) ? $args['name'] : "";
		$value = array_key_exists('value', $args) ? $args['value'] : "";
		$label = array_key_exists('label', $args) ? $args['label'] : "Label not set";
		$checked = array_key_exists('checked', $args) ? $args['checked'] == true ? "checked" : ""  : "";
		//$return .= "<div class='{$class}'>\r\n";
		$this->return .= "<label";
		$labelclass == "" ? "" : $this->return .= " class='{$labelclass}'";
		$this->return .= ">\r\n";
		$this->return .= "<input type='checkbox'";
		$name == "" ? "" : $this->return .= " name='{$name}'";
		$id == "" ? "" : $this->return .= " id='{$id}'";
		$value == "" ? "" : $this->return .= " value='{$value}'";
		$checked != "" ? $this->return .= " checked='checked'" : "";
		$this->return .= "/>\r\n";
		$this->return .= "{$label}\r\n";
		$this->return .= "</label>\r\n";
		//$return .= "</div>\r\n";
		return $this;
	}

	public function select($args){
		$class = "";
		$id = "";
		$name = "";
		$value = "";
		$data = "";
		$class = isset($args['class'])? "form-control ".$args['class'] : "form-control";
		$data = array_key_exists('data',$args) ? $args['data'] : array() ;
		$name = array_key_exists('name', $args) ? $args['name'] : "";
		$selected = array_key_exists('selected', $args) ? $args['selected'] : null;
		$num = count($data) % 2 == 0 ? count($data) : count($data) - 1;
		$this->return .= "<select class='{$class}'";
		$name == "" ? "" : $this->return .= " name='{$name}'";
		$id == "" ? "" : $this->return .= " id='{$id}'";
		$this->return .= ">\r\n";
		for($i = 0; $i < $num; $i++){
			switch ($i%2){
				case 0:
					$this->return .= "<option";
					$this->return .= " value='{$data[$i]}'";
					if($selected  != null && $data[$i] == $selected){
						$this->return .= " selected='selected'";
					}
					$this->return .= ">";
				break;
				case 1:
					$this->return .= "{$data[$i]}</option>\r\n";
				break;
			}
		}
		$this->return .= "</select>\r\n";
		return $this;
	}

	public function form($type,$label = '',$args = array()){
		global $kmf;
		switch($type){
			case "input":
			case "textarea":
			case "select":
				$this->return .= "<div class='form-group";
				if(array_key_exists('name',$args)){
					array_key_exists('Validation', $kmf)? $kmf->Validation->get_error($args['name']) != null? $this->return .= " has-error" : "" : "";
				}
				$this->return .= "'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>";
				$this->$type($args);
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
			case "fileinput":
				$this->return .= "<div class='form-group'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>";
				$this->fileinput($args);
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
			case "radio":
				$this->return .= "<div class='form-group'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>\r\n";
				if(array_key_exists('datas', $args) && is_array($args['datas'])){
					if(array_key_exists('inline', $args) && $args['inline'] == true){
						foreach ($args['datas'] as $data) {
							array_key_exists('labelclass',$data) ? $data['labelclass'] = "radio-inline ".$data['labelclass'] : $data['labelclass'] = "radio-inline" ;
							$this->$type($data);
						}
					} else {
						foreach ($args['datas'] as $data) {
							$this->return .= "<div class='radio'>";
							$this->$type($data);
							$this->return .= "</div>";
						}
					}
				} else {
					$this->return .= "<div class='radio'>";
					$this->$type($args);
					$this->return .= "</div>";
				}
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
			case "checkbox":
				$this->return .= "<div class='form-group'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>\r\n";
				if(array_key_exists('datas', $args) && is_array($args['datas'])){
					if(array_key_exists('inline', $args) && $args['inline'] == true){
						foreach ($args['datas'] as $data) {
							array_key_exists('labelclass',$data) ? $data['labelclass'] = "checkbox-inline ".$data['labelclass'] : $data['labelclass'] = "checkbox-inline" ;
							$this->$type($data);
						}
					} else {
						foreach ($args['datas'] as $data) {
							$this->return .= "<div class='checkbox'>";
							$this->$type($data);
							$this->return .= "</div>";
						}
					}
				} else {
					$this->return .= "<div class='checkbox'>";
					$this->$type($args);
					$this->return .= "</div>";
				}
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
			case "submit":
				$this->return .= "<div class='form-group'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>";
				$this->submit();
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
			default:
				$this->return .= "<div class='form-group'>\r\n";
				$this->return .= "<label class='col-sm-2 control-label'>{$label}</label>\r\n";
				$this->return .= "<div class='col-sm-10'>";
				$this->return .= !empty($args) ? $this->implement($args) : "";
				$this->return .= "</div>\r\n";
				$this->return .= "</div>\r\n";
			break;
		}
		return $this;
	}

	public function submit($args = array()){
		if(array_key_exists('callback', $args) && !empty($args['callback'])){
			$this->$args['callback'] = "";
			$this->$args['callback'] .= "<button type='submit' class='btn btn-primary'>";
			array_key_exists('value',$args)? $this->$args['callback'] .= $args['value'] : $this->$args['callback'] .= "Submit";
			$this->$args['callback'] .= "</button>\r\n";
		} else {
			$this->return .= "<button type='submit' class='btn btn-primary'>";
			array_key_exists('value',$args)? $this->return .= $args['value'] : $this->return .= "Submit";
			$this->return .= "</button>\r\n";
		}
		return $this;
	}

	public function button($args){
		$class = "";
		$callback = "";
		$value = "";
		$id = "";
		$callback = array_key_exists('callback',$args) ? $args['callback'] != "" ? $args['callback'] : "callback" : "";
		$class = array_key_exists('class',$args)? "btn ".$args['class'] : "btn btn-primary";
		$value = array_key_exists('value',$args) ? $args['value'] : "";
		if($callback == ""){
			$this->return .= "<button type='button' class='{$class}'>";
			$this->return .= $value == ""? "Button" : $value ;
			$this->return .= "</button>\r\n";
		} else {
			$this->$callback = "";
			$this->$callback .= "<button type='button' class='{$class}'>";
			$this->$callback .= $value == ""? "Button" : $value ;
			$this->$callback .= "</button>\r\n";
		}
		return $this;
	}

	public function a($args){
		$href = "";
		$class = "";
		$target = "";
		$value = "";
		$implement = "";
		$callback = "";
		$callback = array_key_exists('callback',$args) ? $args['callback'] != "" ? $args['callback'] : "callback" : "";
		$href = array_key_exists('href',$args)? $args['href'] : "";
		$class = array_key_exists('class',$args)? $args['class'] : "";
		$id = array_key_exists('id',$args)? $args['id'] : "";
		$value = array_key_exists('value',$args) ? $args['value'] : "";
		if(preg_match_all('/\{(.+?)\}/', $value,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$value = preg_replace('/\{'.$match.'\}/',$this->$match,$value);
				}
				$i++;
			}
		}
		if($callback != ""){
			$this->$callback = "";
			$this->$callback .= "<a";
			$this->$callback .= $href != "" ? " href='{$href}'" : " href='#'";
			$this->$callback .= $target != ""? " target='{$target}'" : "";
			$this->$callback .= $id != ""? " id='{$id}'" : "";
			$this->$callback .= $class != ""? " class='{$class}'" : "";
			$this->$callback .= ">\r\n";
			$this->$callback .= $value;
			$this->$callback .= "</a>\r\n";
		} else {
			$this->return .= "<a";
			$this->return .= $href != "" ? " href='{$href}'" : " href='#'";
			$this->return .= $target != ""? " target='{$target}'" : "";
			$this->return .= $id != ""? " id='{$id}'" : "";
			$this->return .= $class != ""? " class='{$class}'" : "";
			$this->return .= ">\r\n";
			$this->return .= $value;
			$this->return .= "</a>\r\n";
		}
		return $this;
	}

	public function hr(){
		$this->return .= "<hr />\r\n";
	}

	public function div($args = array()){
		$class = array_key_exists('class',$args)? !empty($args['class'])? $args['class'] : "" : "";
		$id = array_key_exists('id',$args)? !empty($args['id'])? $args['id'] : "" : "";
		$style = array_key_exists('style',$args)? !empty($args['style'])? $args['style'] : "" : "";
		$this->return .= "<div";
		$this->return .= $class != ""? " class='{$class}'" : "";
		$this->return .= $id != ""? " id='{$id}'" : "";
		$this->return .= $style != ""? " style='{$style}'" : "";
		if(array_key_exists('add', $args) && is_array($args['add'])){
			foreach($args['add'] as $key => $value){
				$this->return .= " {$key}='$value'";
			}
		}
		$this->return .= ">\r\n";
	}

	public function div_close(){
		$this->return .= "</div>";
	}

	public function build(){
		$script = "";
		if(!empty($this->script)){
			$script .= "<script>\r\n";
			$script .= $this->script;
			$script .= "</script>\r\n";
		}
		$footer = $this->load_js().$script."</body></html>";
		$this->return = $this->html().$this->return.$footer;
		echo $this->clean_html_code($this->return);
	}

	private function build_css_content() {
		$return = "";
		if(sizeof($this->css_path) > 0) {
			foreach($this->css_path as $path) {
				$return .= "<link rel=\"stylesheet\" href=\"".$path."\">\r\n";
			}
		}
		return $return; 
	}

	private function build_js_content() {
		$return = "";
		if(sizeof($this->js_path) > 0) {
			foreach($this->js_path as $path) {
				$return .= "<script src=\"".$path."\"></script>\r\n";
			}
		}
		return $return; 
	}

	public function fix_newlines_for_clean_html($fixthistext)
	{
		$fixthistext_array = explode("\n", $fixthistext);
		foreach ($fixthistext_array as $unfixedtextkey => $unfixedtextvalue)
		{
			if (!preg_match("/^(\s)*$/", $unfixedtextvalue))
			{
				$fixedtextvalue = preg_replace("/>(\s|\t)*</U", ">\r\n<", $unfixedtextvalue);
				$fixedtext_array[$unfixedtextkey] = $fixedtextvalue;
			}
		}
		return implode("\n", $fixedtext_array);
	}

	private function clean_html_code($uncleanhtml)
	{
		$indent = "    ";

		$fixed_uncleanhtml = $this->fix_newlines_for_clean_html($uncleanhtml);
		$uncleanhtml_array = explode("\n", $fixed_uncleanhtml);
		//Sets no indentation
		$indentlevel = 0;
		foreach ($uncleanhtml_array as $uncleanhtml_key => $currentuncleanhtml)
		{
			$currentuncleanhtml = preg_replace("/\t+/", "", $currentuncleanhtml);
			$currentuncleanhtml = preg_replace("/^\s+/", "", $currentuncleanhtml);
			
			$replaceindent = "";
			for ($o = 0; $o < $indentlevel; $o++)
			{
				$replaceindent .= $indent;
			}
			if (preg_match("/<(.+)\/>/", $currentuncleanhtml))
			{ 
				$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;
			}
			else if (preg_match("/<!(.*)>/", $currentuncleanhtml))
			{ 
				$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;
			}
			else if (preg_match("/<[^\/](.*)>/", $currentuncleanhtml) && preg_match("/<\/(.*)>/", $currentuncleanhtml))
			{ 
				$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;
			}
			else if (preg_match("/<\/(.*)>/", $currentuncleanhtml) || preg_match("/^(\s|\t)*\}{1}(\s|\t)*$/", $currentuncleanhtml))
			{
				$indentlevel--;
				$replaceindent = "";
				for ($o = 0; $o < $indentlevel; $o++)
				{
					$replaceindent .= $indent;
				}
				$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;
			}
			else if ((preg_match("/<[^\/](.*)>/", $currentuncleanhtml) && !preg_match("/<(link|input|meta|base|br|img|hr)(.*)>/", $currentuncleanhtml)) || preg_match("/^(\s|\t)*\{{1}(\s|\t)*$/", $currentuncleanhtml))
			{
				$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;
				
				$indentlevel++;
				$replaceindent = "";
				for ($o = 0; $o < $indentlevel; $o++)
				{
					$replaceindent .= $indent;
				}
			}
			else
			{$cleanhtml_array[$uncleanhtml_key] = $replaceindent.$currentuncleanhtml;}
		}
		return implode("\n", $cleanhtml_array);	
	}

	private function implement($string){
		if(preg_match_all('/\{(.+?)\}/', $string,$matches)){
			$i = 0;
			foreach($matches['1'] as $match){
				if(isset($this->$match) && $this->$match != ""){
					$string = preg_replace('/\{'.$match.'\}/',$this->$match,$string);
				}
				$i++;
			}
		}
		return $string; 
	}

	public function cleantext($string){
		//$string = htmlentities($string, ENT_QUOTES);
		$string = preg_replace('/\{/', '&#123;', $string);
		$string = preg_replace('/\}/', '&#125;', $string); 
		return $string;
	}

}

?>